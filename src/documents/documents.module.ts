
import { Module } from '@nestjs/common';
import { DocumentsController } from './documents.controller';
import { DocumentsService } from './documents.service';

// we need to add the Bull queue to our module in order to register it
// we can use it registerQueue or registerQueueAsync for creating synch or aasynch queues

@Module({
    imports: [],
    controllers: [DocumentsController],
    providers: [DocumentsService],
})
export class DocumentsModule { }
