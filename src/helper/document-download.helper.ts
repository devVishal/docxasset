import { createWriteStream, readFileSync, unlinkSync } from 'fs';
import { get } from 'http'


//Method to download the document/file with the url to the file
export const downloadDocument = (fileAccessUrl) => {

    //http get method to download the file from download url
    get(fileAccessUrl, res => {
        const path = `downloaded-doc-${new Date()}.docx`;
        const writeStream = createWriteStream(path);

        res.pipe(writeStream);

        writeStream.on('finish', () => {
            writeStream.close();
            console.log('Download Completed');
            return { message: 'document downloaded', name: path }
        });
    });

}


// Download the file and read it into base64 string to be attached in a email template
export const docDownloadBase64 = async (fileAccessUrl) => {
    //This shall store our converted document into base64 format
    let base64DocumentString = '';

    //http get method to download the file from the download url
    await get(fileAccessUrl, res => {
        const path = `downloaded-doc-${new Date()}.docx`;
        const writeStream = createWriteStream(path);

        res.pipe(writeStream);

        writeStream.on('finish', () => {
            writeStream.close();
            console.log('Download Completed');
            // read the file from fs as a base64 string
            base64DocumentString = readFileSync(path, {
                encoding: 'base64',
            });
            // remove the file from disk to save on resources
            unlinkSync(path);
            //call the helper method to send the email and pass
            // the base64 string (base64DocumentString) to the
            // helper method and attach in the email template

            console.log('Sendimg email');
            // sendDocumentMail() is the method to be used for shooting emails.
            // we need to pass the required data as per use case to the helper method
            const mailResp = sendDocumentMail({
                email: 'vishal.kaushal@mtxb2b.com , vishalkaushal32@gmail.com',
                cc: ['vishal.kaushal+1@mtxb2b.com', 'apurbo.mitra@mtxb2b.com'],
                docUrl: base64DocumentString,

            });
            console.log('Email sent', mailResp);

        });

    })
    return { status: 'mail sent' }

}


// helper method to send email
// typically this would prepare template and send email using clients like SendGrid
// this is usually a async method because of the nature of email sending feature
const sendDocumentMail = async ({
    email,
    cc,
    docUrl }) => {
    return {
        status: 'mock email sent',
        emailTo: email,
        ccTo: cc,
        docUrlBase64: docUrl
    }

}